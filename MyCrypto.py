from Crypto.Signature import PKCS1_v1_5
from Crypto.Hash import SHA
from Crypto.Cipher import AES, PKCS1_OAEP
from Crypto.PublicKey import RSA
from Crypto import Random
import settings


class MyCrypto:
    @staticmethod
    def generate_rsa_key():
        RSA_key = RSA.generate(settings.RSA_TOKEN)
        private_key = RSA_key.exportKey('PEM')
        public_key = RSA_key.publickey().exportKey('PEM')
        return {'private': private_key, 'public': public_key}

    @staticmethod
    def create_signature(text, private_key):
        myhash = SHA.new(text)
        signature = PKCS1_v1_5.new(RSA.importKey(private_key))
        return signature.sign(myhash)

    @staticmethod
    def encrypt_signature(sig, public_key):
        publickey = RSA.importKey(public_key)
        cipherrsa = PKCS1_OAEP.new(publickey)
        return cipherrsa.encrypt(sig[:128]) + cipherrsa.encrypt(sig[128:])

    @staticmethod
    def create_session():
        return Random.new().read(settings.SESSION_KEY_LENGTH)

    @staticmethod
    def AES_ecnrypt(text, session_key):
        iv = Random.new().read(16)  # 128 bit
        obj = AES.new(session_key, AES.MODE_CFB, iv)
        return iv + obj.encrypt(text)

    @staticmethod
    def RSA_ecnrypt(public_key, session_key):
        public_key = RSA.importKey(public_key)
        cipherrsa = PKCS1_OAEP.new(public_key)
        return cipherrsa.encrypt(session_key)

    @staticmethod
    def decrypt_session(private_key, session_key):
        private_key = RSA.importKey(private_key)
        cipherrsa = PKCS1_OAEP.new(private_key)
        return cipherrsa.decrypt(session_key)

    @staticmethod
    def decrypt_message(message, session_key):
        iv = message[:16]
        obj = AES.new(session_key, AES.MODE_CFB, iv)
        return obj.decrypt(message)[16:]

    @staticmethod
    def decrypt_signature(private_key, sig):
        private_key = RSA.importKey(private_key)
        cipherrsa = PKCS1_OAEP.new(private_key)
        return cipherrsa.decrypt(sig[:256]) + cipherrsa.decrypt(sig[256:])

    @staticmethod
    def verify_signature(text, sig, public_key):
        publickey = RSA.importKey(public_key)
        myhash = SHA.new(text)
        signature = PKCS1_v1_5.new(publickey)
        return signature.verify(myhash, sig)


if __name__ == '__main__':
    # каждый ключ имеет паблик и прайвет часть. стороны в начале обмениваются своими паблик частями ключкей,
    # я это уже сделал вроде.
    keys_a = MyCrypto.generate_rsa_key()  # key generation Alisa
    keys_b = MyCrypto.generate_rsa_key()  # key generation Bob
    print(keys_b['public'])
    #  сервер генерит сессию, зашифрованная сессия должа передаваться с сообщениями постоянно.
    sessionkey = MyCrypto.create_session()  # creation 256 bit session key

    message1 = 'ALISA SSANAIA'.encode()  # шоб всё работало текст надо сначало ебнуть в .encode()

    signature = MyCrypto.create_signature(message1, keys_a['private'])  # creation of signature
    encrypted_sig = MyCrypto.encrypt_signature(message1, keys_b['public'])  # signature encrypt

    # перед оправкой шифруем: сообщение шифруем сессией, сессию шифруем паблик ключем получателя (он её сможет
    # расшифровать своим приватным ключем)
    encrypted_msg = MyCrypto.AES_ecnrypt(message1, sessionkey)  # encryption AES of the message
    encrypted_sessionkey = MyCrypto.RSA_ecnrypt(keys_b['public'], sessionkey)  # encryption RSA of the session key

    # передаем  encrypted_msg и encrypted_sessionkey

    # получатель расшифровует сессию своим приватным ключем
    sessionkey = MyCrypto.decrypt_session(keys_b['private'], encrypted_sessionkey)  # decryption session key
    # получатель расшифровует сообщение сессией
    message2 = MyCrypto.decrypt_message(encrypted_msg, sessionkey)  # decryption message

    sig = MyCrypto.decrypt_signature(keys_b['private'], encrypted_sig)  # decryption signature
    test = MyCrypto.verify_signature(message2, sig, keys_a['public'])  # signature verification

    # осталось только трансформировать текст из бинарного вида в обычный
    print(message1.decode(), message2.decode())