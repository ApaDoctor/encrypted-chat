#! /usr/bin/env python3
import socket
import datetime
import sys
import threading
from MyCrypto import MyCrypto

import settings


def log(message):
    """
    Logging function check, print logs if setting Logging is Equeal to True
    """
    if settings.LOGGING:
        print("[%s LOG]" % datetime.datetime.now().strftime("%Y-%m-%d %H:%M"), message)


class ChatServer:
    """
    Chat server class.
    Init new server at the port with max_connections
    """

    def __init__(self, port=settings.COMMUNICATION_PORT, max_connections=settings.MAX_CONECTIONS, ):
        self.max_connections = max_connections
        self.port = port
        self.name = input("Type your name: \n")

        self.signature = MyCrypto.create_signature(message1, keys_a['private'])  # creation of signature
        self.encrypted_sig = MyCrypto.encrypt_signature(message1, keys_b['public'])  # signature encrypt

        self.key = MyCrypto.generate_rsa_key()  # generating public and private keys
        self.session_key = MyCrypto.create_session()  # generating session

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        log("Binding port")
        self.socket.bind(('', settings.COMMUNICATION_PORT))  # binding socket

        self._connection = False
        self._server_can_run = True

    def _listen(self):
        """
        Waiting for connection of the client.
        After that sending public key and waiting for public key of client
        :return: True when client connected
        """
        log("Waiting for connection...")
        self.socket.listen(self.max_connections)
        self._connection = self.socket.accept()[0]
        log("Client connected\n")

        log("Waiting for client public key")
        self.client_pub_key = self._recieve_data()
        log("Public key received: {}".format(self.client_pub_key))

        log("Sending server public key")
        self._send_data(self.key['public'])
        return True

    def _start_meshandl_server(self):
        """
        Message handling server.
        Waiting for messages from client.
        Decrypting session key with own private key.
        And then decrypting message with decrypted session key.
        Then printing message to the stdout
        """
        log("Starting message-handling server")
        while self._connection:
            data = self._connection.recv(settings.BUFFER_SIZE).split(settings.SEPARATOR.encode())
            try:
                log("Received encrypted message and encrypted session key:\nMessage:{}\n\nSession key:{}\n\n".format(
                    data[0], data[1]))
            except IndexError:
                break

            if not data: break

            log("Decrypting session with private key")
            session_key = MyCrypto.decrypt_session(self.key['private'], data[1])
            log("Session key decrypted: {}".format(session_key))
            log("Decrypting message")
            msg = MyCrypto.decrypt_message(data[0], session_key)
            log("Message decrypted")
            print(msg.decode())

        log("Message-handling server has stoped")
        self.socket.close()
        sys.exit(0)

    def _start_inputnahdl_server(self):
        """
        Input handling server.
        Waiting for input from keyboard.
        Encrypting message, sending it with encrypted session key.
        """
        log("Starting input-handling server")
        print("Now you can input your messages\nTo exit program click Ctrl+C\n\n")

        while self._server_can_run:
            msg = ("%s: %s" % (self.name, input())).encode()
            log("Creating signature")
            signature = MyCrypto.create_signature(msg, self.key['private'])
            log("Encrypting signature")
            encrypted_sig = MyCrypto.encrypt_signature(signature, self.client_pub_key)  # signature encrypt
            log("Encrypting message")
            msg = MyCrypto.AES_ecnrypt(msg, self.session_key)  # Encrypting message
            log("Message encrypted:\n{}\n".format(msg))
            log("Encrypting session key:\n{}\n".format(self.session_key))
            encrypted_seskey = MyCrypto.RSA_ecnrypt(self.client_pub_key, self.session_key)
            log("Encrypted:\n{}\n".format(encrypted_seskey))
            log("Sending encrypted message, encrypted session key pair")
            self._connection.send(msg + settings.SEPARATOR.encode() + encrypted_seskey)  # Sending encrypted message

    def _send_data(self, data):
        """
        Send data via socket
        """
        self._connection.send(data)

    def _recieve_data(self):
        """
        Receive data via socket
        """
        data = self._connection.recv(settings.BUFFER_SIZE)

        return data

    def start_server(self):
        """
        Public method of server starting
        Init two threads, first one for message handling server,
        second for input handling server
        """
        self._listen()
        if not self._connection:
            sys.stderr.write("Connection isn't set. Server can't be started.")
        t1 = threading.Thread(target=self._start_meshandl_server)
        t2 = threading.Thread(target=self._start_inputnahdl_server)
        t1.start()
        t2.start()
        t1.join()
        t2.join()

    def __del__(self):
        self.socket.close()


class ChatClient:
    def __init__(self, port=settings.COMMUNICATION_PORT):
        """
        Creates session, private-public key pair
        Sending public key to the server, and receiving public key from server.
        :param: port - server port
        """
        self.port = port
        self.session_key = MyCrypto.create_session()
        self.key = MyCrypto.generate_rsa_key()

        log("Connecting to the server")

        self.name = input("Type your name: \n")
        self._connection = socket.socket()
        try:
            self._connection.connect(('localhost', settings.COMMUNICATION_PORT))
        except ConnectionRefusedError:
            print("No running server at that port")
            log("Stopping client...")
            sys.exit(1)
        log("Connected")

        log("Sending public key")
        self._send_data(self.key['public'])
        log("Waiting for server public key")
        self.server_public_key = self._receive_data()
        log("Public key received:\n{}".format(self.server_public_key.decode()))

    def _send_message_server(self):
        """
        Input handling server.
        Waiting for input from keyboard.
        Encrypting message, sending it with encrypted session key.
        """

        print("Now you can input your messages\nTo exit program click Ctrl+C\n\n")
        while True:
            msg = ("%s: %s" % (self.name, input())).encode()
            log("Creating signature")
            signature = MyCrypto.create_signature(msg, self.key['private'])
            log("Encrypting signature")
            encrypted_sig = MyCrypto.encrypt_signature(signature, self.server_public_key)  # signature encrypt
            log("Encrypting message")
            msg = MyCrypto.AES_ecnrypt(msg, self.session_key)  # Encrypting message
            log("Message encrypted:\n{}\n".format(msg))
            log("Encrypting session key:\n{}\n".format(self.session_key))
            encrypted_seskey = MyCrypto.RSA_ecnrypt(self.server_public_key, self.session_key)
            log("Encrypted:\n{}\n".format(encrypted_seskey))
            log("Sending encrypted message, encrypted session key pair")
            self._connection.send(msg + settings.SEPARATOR.encode() + encrypted_seskey)  # Sending encrypted message

    def _receive_messages_server(self):
        """
        Message handling server.
        Waiting for messages from server.
        Decrypting session key with own private key.
        And then decrypting message with decrypted session key.
        Then printing message to the stdout
        """
        while True:
            data = self._connection.recv(settings.BUFFER_SIZE).split(settings.SEPARATOR.encode())
            log("Received encrypted message and encrypted session key:\nMessage:{}\n\nSession key:{}\n\n".format(
                data[0], data[1]))
            if not data: break

            log("Decrypting session with private key")
            session_key = MyCrypto.decrypt_session(self.key['private'], data[1])
            log("Session key decrypted: {}".format(session_key))
            log("Decrypting message")
            msg = MyCrypto.decrypt_message(data[0], session_key)
            log("Message decrypted")
            print(msg.decode())

        log("Connection has closed")
        sys.exit(1)

    def _send_data(self, data):
        self._connection.send(data)

    def _receive_data(self):
        data = self._connection.recv(settings.BUFFER_SIZE)
        return data

    def start_server(self):
        """
        Public method of server starting
        Init two threads, first one for message handling server,
        second for input handling server
        """
        t1 = threading.Thread(target=self._send_message_server)
        t2 = threading.Thread(target=self._receive_messages_server)
        for x in t1, t2:
            x.start()


if __name__ == "__main__":
    if "--chat" in sys.argv or "-c" in sys.argv:
        try:
            client = ChatClient()
            client.start_server()
        except KeyboardInterrupt:
            print("\nGot KeyboardInterrupt. Program will exit")
            sys.exit(0)
    else:
        try:
            server = ChatServer()
            server.start_server()
        except KeyboardInterrupt:
            print("\nGot KeyboardInterrupt. Program will exit")
            sys.exit(0)
