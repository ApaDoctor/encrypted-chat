# settings.py
# There are settings to configure Encrypted Chat

MAX_CONECTIONS = 1  # Do not change it.
COMMUNICATION_PORT = 9225
BUFFER_SIZE = 1024
LOGGING = False
RSA_TOKEN = 2048
SESSION_KEY_LENGTH = 32
SEPARATOR = ";;;;;"